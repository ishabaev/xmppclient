package com.ishabaev.xmppclient;

import android.os.Handler;

import com.ishabaev.xmppclient.dao.OrmMessageItem;
import com.ishabaev.xmppclient.data.MessageItem;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smackx.chatstates.ChatState;
import org.jivesoftware.smackx.chatstates.ChatStateListener;
import org.jivesoftware.smackx.chatstates.ChatStateManager;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.subjects.PublishSubject;

/**
 * Created by ishabaev on 13.07.16.
 */
public class XmppChatManager implements ChatManagerListener {

    private ChatManager mChatmanager;
    private ChatStateManager mChatStateManager;
    private Handler mHandler = new Handler();
    private static XmppChatManager INSTANCE;
    PublishSubject<MessageItem> subject = PublishSubject.create();
    PublishSubject<String> stateSubject = PublishSubject.create();
    private Map<String, Chat> chats = new HashMap<>();

    private XmppDatabase mDatabase;

    public static XmppChatManager getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new XmppChatManager();
        }
        return INSTANCE;
    }

    public void setDatabase(XmppDatabase database) {
        mDatabase = database;
    }

    public void initChatManager(AbstractXMPPConnection connection) {
        mChatmanager = ChatManager.getInstanceFor(connection);
        mChatmanager.addChatListener(this);
        mChatStateManager = ChatStateManager.getInstance(connection);
    }

    public Subscription subscribe2message(Observer<? super MessageItem> observer) {
        return subject.subscribe(observer);
    }


    public Subscription subscribe2message(final Action1<? super MessageItem> onNext, final Action1<Throwable> onError, final Action0 onComplete) {
        if (onNext == null) {
            throw new IllegalArgumentException("onNext can not be null");
        }
        if (onError == null) {
            throw new IllegalArgumentException("onError can not be null");
        }
        if (onComplete == null) {
            throw new IllegalArgumentException("onComplete can not be null");
        }

        return subject.subscribe(new Subscriber<MessageItem>() {

            @Override
            public final void onCompleted() {
                onComplete.call();
            }

            @Override
            public final void onError(Throwable e) {
                onError.call(e);
            }

            @Override
            public final void onNext(MessageItem args) {
                onNext.call(args);
            }

        });
    }


    public Subscription subscribe2state(Observer<? super String> observer) {
        return stateSubject.subscribe(observer);
    }


    public Subscription subscribe2state(final Action1<? super String> onNext, final Action1<Throwable> onError, final Action0 onComplete) {
        if (onNext == null) {
            throw new IllegalArgumentException("onNext can not be null");
        }
        if (onError == null) {
            throw new IllegalArgumentException("onError can not be null");
        }
        if (onComplete == null) {
            throw new IllegalArgumentException("onComplete can not be null");
        }

        return stateSubject.subscribe(new Subscriber<String>() {

            @Override
            public final void onCompleted() {
                onComplete.call();
            }

            @Override
            public final void onError(Throwable e) {
                onError.call(e);
            }

            @Override
            public final void onNext(String args) {
                onNext.call(args);
            }

        });
    }

    public void beginChat(String chatName) {
        Chat chat = mChatmanager.createChat(chatName);
        chats.put(chatName, chat);
    }

    public void sendMsg(String chatName, String message) throws SmackException.NotConnectedException {
        Chat chat = chats.get(chatName);
        if (chat != null) {
            chat.sendMessage(message);
        }
    }

    public void setTyping(ChatState state, String chatName) throws SmackException.NotConnectedException {
        Chat chat = chats.get(chatName);
        if (chat != null) {
            mChatStateManager.setCurrentState(state, chat);
        }
    }

    public void beginGroupChat(String chat, MessageListener listener) {
        /*if (connection.isConnected() == true) {
            //newChat = mChatmanager.createChat("ishabaev@jabber.ru");
            mMultiUserChat = mMultiUserChatmanager.getMultiUserChat(chat);
            mMultiUserChat.addMessageListener(listener);
        }*/
    }

    public void closeChat(String chatName) {
        Chat chat = chats.get(chatName);
        if (chat != null) {
            chat.close();
        }
    }

    @Override
    public void chatCreated(Chat chat, boolean createdLocally) {
        chat.addMessageListener(new ChatStateListener() {
            @Override
            public void stateChanged(Chat chat, ChatState state) {
                if (state.equals(ChatState.composing)) {
                    String[] split = chat.getParticipant().split("@");
                    String userName = "";
                    if (split.length > 0) {
                        userName = split[0];
                    }
                    final String res = userName + " typing...";
                    mHandler.post(() -> stateSubject.onNext(res));
                } else {
                    mHandler.post(() -> stateSubject.onNext(""));
                }
            }

            @Override
            public void processMessage(Chat chat, Message message) {
                if (mHandler == null || message.getBody() == null) {
                    return;
                }
                final MessageItem messageItem = new MessageItem();
                messageItem.setMessage(message.getBody());
                String[] from = message.getFrom().split("/");
                if (from.length > 0) {
                    messageItem.setUserName(from[0]);
                } else {
                    messageItem.setUserName("none");
                }

                messageItem.setMessageDate(Calendar.getInstance().getTime());
                mHandler.post(() -> subject.onNext(messageItem));
                if (mDatabase != null) {
                    OrmMessageItem ormMessageItem = new OrmMessageItem();
                    ormMessageItem.setChatName(from[0]);
                    ormMessageItem.setUserName(from[0]);
                    ormMessageItem.setMessage(messageItem.getMessage());
                    ormMessageItem.setMessageTime(messageItem.getMessageDate());
                    mDatabase.saveOrmMessageItem(ormMessageItem);
                }
            }
        });
    }
}
