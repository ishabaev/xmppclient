package com.ishabaev.xmppclient;

import android.content.Context;
import android.util.Log;

import com.ishabaev.xmppclient.dao.OrmUser;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.roster.RosterListener;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.chatstates.ChatStateManager;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jivesoftware.smackx.ping.PingManager;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;

import java.io.IOException;
import java.util.Collection;

import rx.Observable;
import rx.Subscriber;

/**
 * Created by ishabaev on 09.07.16.
 */
public class XmppClient {

    private AbstractXMPPConnection mConnection;
    private ChatStateManager mStateManager;
    MultiUserChatManager mMultiUserChatmanager;
    MultiUserChat mMultiUserChat;

    private static XmppClient INSTANCE;
    private Roster mRoster;

    private XmppClient() {

    }

    public static XmppClient getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new XmppClient();
        }
        return INSTANCE;
    }

    public void connect(final String userName, final String passWord, ConnectionListener listener, Context context) throws SmackException, IOException, XMPPException {
        Log.i("XMPP", "Initializing!");
        XMPPTCPConnectionConfiguration.Builder configBuilder = XMPPTCPConnectionConfiguration.builder();
        String[] split = userName.split("@");
        configBuilder.setUsernameAndPassword(split[0], passWord);
        configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        configBuilder.setServiceName(split[1]);
        configBuilder.setHost(split[1]);
        configBuilder.setSendPresence(true);
        configBuilder.setResource("Android");
        configBuilder.setDebuggerEnabled(true);
        XMPPTCPConnectionConfiguration configuration = configBuilder.build();
        mConnection = new XMPPTCPConnection(configuration);
        mConnection.addConnectionListener(listener);
        mConnection.setPacketReplyTimeout(10000);
        PingManager pingManager = PingManager.getInstanceFor(mConnection);
        pingManager.setPingInterval(60 * 8);
        pingManager.registerPingFailedListener(() -> {
            Log.v("Xmpp", "Ping failed");
        });
        ReconnectionManager reconnectionManager = ReconnectionManager.getInstanceFor(mConnection);
        reconnectionManager.setEnabledPerDefault(true);
        reconnectionManager.enableAutomaticReconnection();
        //mConnection.disconnect();
        mRoster = Roster.getInstanceFor(mConnection);
        mRoster.setDefaultSubscriptionMode(Roster.SubscriptionMode.manual);
        XmppRosterListener rosterListener = new XmppRosterListener(mConnection, XmppDatabase.getInstance(context));
        mRoster.addRosterListener(rosterListener);


        //ProviderManager.addIQProvider("vCard", "vcard-temp", new VCardProvider());


        mConnection.connect();
        mConnection.login(split[0], passWord);
        XmppChatManager.getInstance().initChatManager(mConnection);
    }

    public void disconnect() {
        mConnection.disconnect();
    }

    /*
    public Observable<Collection<RosterEntry>> getRosters() {
        return Observable.create(new Observable.OnSubscribe<Collection<RosterEntry>>() {
            @Override
            public void call(Subscriber<? super Collection<RosterEntry>> subscriber) {
                try {
                    if (!mRoster.isLoaded()) {
                        mRoster.reloadAndWait();
                    }
                    Collection<RosterEntry> entries = mRoster.getEntries();
                    subscriber.onNext(entries);
                } catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }
            }
        });
    }
    */
}
