package com.ishabaev.xmppclient;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.ReconnectionManager;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smack.roster.Roster;
import org.jivesoftware.smack.roster.RosterEntry;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnectionConfiguration;
import org.jivesoftware.smackx.chatstates.ChatStateManager;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;

import java.io.IOException;
import java.util.Collection;

import rx.Observable;
import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.subjects.PublishSubject;

/**
 * Created by ishabaev on 09.07.16.
 */
public class XmppClientv2 implements ConnectionListener {

    private AbstractXMPPConnection mConnection;
    private ChatStateManager mStateManager;

    MultiUserChatManager mMultiUserChatmanager;
    MultiUserChat mMultiUserChat;
    private boolean mActive;

    private Thread mThread;
    private Handler mHandler = new Handler();

    PublishSubject<String> subject = PublishSubject.create();

    private static XmppClientv2 INSTANCE;

    private XmppClientv2() {

    }

    public Subscription subscribe(Observer<? super String> observable) {
        return subject.subscribe(observable);
    }

    public Subscription subscribe(final Action1<? super String> onNext, final Action1<Throwable> onError, final Action0 onComplete) {
        if (onNext == null) {
            throw new IllegalArgumentException("onNext can not be null");
        }
        if (onError == null) {
            throw new IllegalArgumentException("onError can not be null");
        }
        if (onComplete == null) {
            throw new IllegalArgumentException("onComplete can not be null");
        }

        return subject.subscribe(new Subscriber<String>() {

            @Override
            public final void onCompleted() {
                onComplete.call();
            }

            @Override
            public final void onError(Throwable e) {
                onError.call(e);
            }

            @Override
            public final void onNext(String args) {
                onNext.call(args);
            }

        });
    }

    public static XmppClientv2 getInstance() {
        if (INSTANCE == null) {
            INSTANCE = new XmppClientv2();
        }
        return INSTANCE;
    }

    public void init(final String userName, final String passWord, final ChatManagerListener listener) throws SmackException, IOException, XMPPException {

        if (!mActive) {
            mActive = true;
            if (mThread == null || !mThread.isAlive()) {
                mThread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Looper.prepare();
                        try {
                            start(userName, passWord, listener);
                        } catch (SmackException | IOException | XMPPException e) {
                            e.printStackTrace();
                            mActive = false;
                            mConnection.disconnect();
                            Looper myLooper = Looper.myLooper();
                            if (myLooper != null) {
                                myLooper.quit();
                            }
                            mHandler.post(new Runnable() {
                                @Override
                                public void run() {
                                    subject.onNext(e.getMessage());
                                }
                            });
                            return;
                        }

                        Looper.loop();

                    }
                });
                mThread.start();
            }
        } else {
            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    subject.onNext("");
                    return;
                }
            });
        }
    }

    public AbstractXMPPConnection getConnection() {
        return mConnection;
    }

    private void start(String userName, String passWord, final ChatManagerListener chatManagerListener) throws SmackException, IOException, XMPPException {
        Log.i("XMPP", "Initializing!");
        XMPPTCPConnectionConfiguration.Builder configBuilder = XMPPTCPConnectionConfiguration.builder();
        String[] split = userName.split("@");
        configBuilder.setUsernameAndPassword(split[0], passWord);
        configBuilder.setSecurityMode(ConnectionConfiguration.SecurityMode.disabled);
        configBuilder.setServiceName(split[1]);
        configBuilder.setHost(split[1]);
        configBuilder.setDebuggerEnabled(true);
        XMPPTCPConnectionConfiguration configuration = configBuilder.build();
        mConnection = new XMPPTCPConnection(configuration);
        mConnection.addConnectionListener(this);
        ReconnectionManager reconnectionManager = ReconnectionManager.getInstanceFor(mConnection);
        reconnectionManager.setEnabledPerDefault(true);
        reconnectionManager.enableAutomaticReconnection();
        mConnection.disconnect();
        mConnection.connect();
        mConnection.login(split[0], passWord);
        XmppChatManager.getInstance().initChatManager(mConnection);
    }

    public Observable<Collection<RosterEntry>> getRosters() {
        return Observable.create(new Observable.OnSubscribe<Collection<RosterEntry>>() {
            @Override
            public void call(Subscriber<? super Collection<RosterEntry>> subscriber) {
                try {
                    Roster roster = Roster.getInstanceFor(mConnection);
                    if (!roster.isLoaded()) {
                        roster.reloadAndWait();
                    }
                    Collection<RosterEntry> entries = roster.getEntries();
                    subscriber.onNext(entries);
                } catch (Exception e) {
                    e.printStackTrace();
                    subscriber.onError(e);
                }
            }
        });
    }

    @Override
    public void connected(final XMPPConnection connection) {
        Log.d("xmpp", "Connected!");
    }

    @Override
    public void connectionClosed() {
        Log.d("xmpp", "Closed!");
    }

    @Override
    public void connectionClosedOnError(Exception arg0) {
        Log.d("xmpp", "ConnectionClosedOn Error!");
    }

    @Override
    public void reconnectingIn(int arg0) {
        Log.d("xmpp", "Reconnectingin " + arg0);
    }

    @Override
    public void reconnectionFailed(final Exception arg0) {
        Log.d("xmpp", "ReconnectionFailed!");
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                subject.onNext(arg0.getMessage());
            }
        });
    }

    @Override
    public void reconnectionSuccessful() {
        Log.d("xmpp", "ReconnectionSuccessful");
    }

    @Override
    public void authenticated(XMPPConnection arg0, boolean arg1) {
        Log.d("xmpp", "Authenticated!");
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                subject.onNext("");
                return;
            }
        });

    }

    public void disconnect() {
        mActive = false;
        mConnection.disconnect();
    }
}
