package com.ishabaev.xmppclient;

import android.content.Context;

import com.ishabaev.xmppclient.dao.DaoMaster;
import com.ishabaev.xmppclient.dao.DaoSession;
import com.ishabaev.xmppclient.dao.OrmMessageItem;
import com.ishabaev.xmppclient.dao.OrmMessageItemDao;
import com.ishabaev.xmppclient.dao.OrmUser;
import com.ishabaev.xmppclient.dao.OrmUserDao;

import java.util.List;

import de.greenrobot.dao.database.Database;
import rx.Observable;
import rx.Subscriber;

/**
 * Created by ishabaev on 13.07.16.
 */
public class XmppDatabase {

    private static XmppDatabase INSTANCE;

    private DaoSession mDaoSession;

    private XmppDatabase(Context context) {
        /*
        DaoMaster.DevOpenHelper devOpenHelper = new DaoMaster.DevOpenHelper(context, "db", null);
        DaoMaster daoMaster = new DaoMaster(devOpenHelper.getWritableDatabase());
        mDaoSession = daoMaster.newSession();
        */


        Database db = new DaoMaster.EncryptedOpenHelper(context, "db") {
            @Override
            public void onUpgrade(Database db, int oldVersion, int newVersion) {
                DaoMaster.dropAllTables(db, true);
                DaoMaster.createAllTables(db, true);
            }
        }.getReadableDatabase("123456");
        mDaoSession = new DaoMaster(db).newSession();
    }

    public static XmppDatabase getInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE = new XmppDatabase(context);
        }
        return INSTANCE;
    }

    public Observable<List<OrmMessageItem>> getMessages(final String chatName) {
        return Observable.create(
                new Observable.OnSubscribe<List<OrmMessageItem>>() {
                    @Override
                    public void call(Subscriber<? super List<OrmMessageItem>> sub) {
                        OrmMessageItemDao messageItemDao = mDaoSession.getOrmMessageItemDao();
                        List<OrmMessageItem> forecast = messageItemDao.queryBuilder()
                                .where(OrmMessageItemDao.Properties.ChatName.eq(chatName))
                                .orderDesc(OrmMessageItemDao.Properties.MessageTime)
                                .build()
                                .list();
                        sub.onNext(forecast);
                        sub.onCompleted();
                    }
                }
        );
    }

    public Observable<List<OrmUser>> getOrmUsers() {
        return Observable.create(
                new Observable.OnSubscribe<List<OrmUser>>() {
                    @Override
                    public void call(Subscriber<? super List<OrmUser>> sub) {
                        OrmUserDao ormUserDao = mDaoSession.getOrmUserDao();
                        List<OrmUser> users = ormUserDao.loadAll();
                        sub.onNext(users);
                        sub.onCompleted();
                    }
                }
        );
    }

    public Observable<OrmUser> getOrmUser(String user) {
        return Observable.create(
                new Observable.OnSubscribe<OrmUser>() {
                    @Override
                    public void call(Subscriber<? super OrmUser> sub) {
                        OrmUserDao ormUserDao = mDaoSession.getOrmUserDao();
                        List<OrmUser> users = ormUserDao.queryBuilder()
                                .where(OrmUserDao.Properties.User.eq(user))
                                .build()
                                .list();
                        if (users.size() > 0) {
                            sub.onNext(users.get(0));
                        }
                        sub.onCompleted();
                    }
                }
        );
    }

    public void saveOrmMessageItem(OrmMessageItem item) {
        OrmMessageItemDao messageItemDao = mDaoSession.getOrmMessageItemDao();
        messageItemDao.insert(item);
    }

    public void saveOrmUser(OrmUser user) {
        OrmUserDao ormUserDao = mDaoSession.getOrmUserDao();
        ormUserDao.queryBuilder()
                .where(OrmUserDao.Properties.User.eq(user.getUser()))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();
        ormUserDao.insertOrReplace(user);
    }

    public void deleteOrmUser(String user) {
        OrmUserDao ormUserDao = mDaoSession.getOrmUserDao();
        ormUserDao.queryBuilder()
                .where(OrmUserDao.Properties.User.eq(user))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();
    }
}
