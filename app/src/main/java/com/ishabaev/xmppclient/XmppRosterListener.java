package com.ishabaev.xmppclient;

import com.ishabaev.xmppclient.dao.OrmUser;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.roster.RosterListener;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;

import java.util.Collection;

/**
 * Created by ishabaev on 16.07.16.
 */
public class XmppRosterListener implements RosterListener {

    private AbstractXMPPConnection mConnection;
    private VCardManager mVCardManager;
    private XmppDatabase mDatabase;

    public XmppRosterListener(AbstractXMPPConnection connection, XmppDatabase database) {
        mConnection = connection;
        mVCardManager = VCardManager.getInstanceFor(mConnection);
        mDatabase = database;
    }

    @Override
    public void presenceChanged(Presence presence) {
        String from = presence.getFrom();
        String[] components = from.split("/");
        OrmUser ormUser = new OrmUser();
        ormUser.setUser(components[0]);
        if (components.length > 1) {
            ormUser.setResource(components[1]);
        }
        try {
            VCard vCard = mVCardManager.loadVCard("ishabaev@jabber.ru");
            if (vCard != null) {
                byte[] avatar = vCard.getAvatar();
                if (avatar != null) {
                    ormUser.setAvatar(avatar);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        mDatabase.saveOrmUser(ormUser);
    }

    public void entriesAdded(Collection<String> param) {
        for (String adress : param) {
            try {
                Presence pres = new Presence(Presence.Type.subscribe);
                pres.setFrom(mConnection.getUser());
                pres.setTo(adress);
                mConnection.sendStanza(pres);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void entriesDeleted(Collection<String> addresses) {
        for (String address : addresses) {
            String[] components = address.split("/");
            mDatabase.deleteOrmUser(components[0]);
        }
    }

    public void entriesUpdated(Collection<String> addresses) {
        for (String adress : addresses) {
            try {
                Presence pres = new Presence(Presence.Type.subscribe);
                pres.setFrom(mConnection.getUser());
                pres.setTo(adress);
                mConnection.sendStanza(pres);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
