package com.ishabaev.xmppclient;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.ishabaev.xmppclient.chat.ChatActivity;
import com.ishabaev.xmppclient.data.MessageItem;
import com.ishabaev.xmppclient.utils.ImageUtils;

import org.jivesoftware.smack.ConnectionListener;
import org.jivesoftware.smack.XMPPConnection;

import rx.Observer;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Action1;
import rx.schedulers.Schedulers;
import rx.subjects.PublishSubject;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by ishabaev on 14.07.16.
 */
public class XmppService extends Service implements ConnectionListener {

    public class XmppBinder extends Binder {
        public XmppService getService() {
            return XmppService.this;
        }
    }

    private XmppClient mClient;
    private XmppChatManager mChatManager;
    private XmppDatabase mDatabase;
    private Thread mThread;
    private Handler mHandler;
    private boolean mAuthenticated;
    private NotificationManager notificationManager;
    private CompositeSubscription mSubscriptions;
    private boolean mBinded;
    private ImageUtils mImageUtils;

    private PublishSubject<String> mConnectionSubject = PublishSubject.create();

    public Subscription subscribe(Observer<? super String> observable) {
        return mConnectionSubject.subscribe(observable);
    }

    public Subscription subscribe(final Action1<? super String> onNext, final Action1<Throwable> onError, final Action0 onComplete) {
        if (onNext == null) {
            throw new IllegalArgumentException("onNext can not be null");
        }
        if (onError == null) {
            throw new IllegalArgumentException("onError can not be null");
        }
        if (onComplete == null) {
            throw new IllegalArgumentException("onComplete can not be null");
        }

        return mConnectionSubject.subscribe(new Subscriber<String>() {

            @Override
            public final void onCompleted() {
                onComplete.call();
            }

            @Override
            public final void onError(Throwable e) {
                onError.call(e);
            }

            @Override
            public final void onNext(String args) {
                onNext.call(args);
            }

        });
    }

    public XmppService() {
        //mClient = XmppClient.getInstance();
        //mChatManager = XmppChatManager.getInstance();
        //mDatabase = XmppDatabase.getInstance(getApplicationContext());
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mClient = XmppClient.getInstance();
        mChatManager = XmppChatManager.getInstance();
        mDatabase = XmppDatabase.getInstance(getApplicationContext());
        mChatManager.setDatabase(mDatabase);
        mHandler = new Handler();
        mSubscriptions = new CompositeSubscription();
        subsrcubeToMessages();
        Log.d("XmppService", "onCreate!");
        mImageUtils = new ImageUtils();
        /*
        AccountManager am = AccountManager.get(this);
        Account[] accounts = am.getAccountsByType(XmppAccount.TYPE);
        if(accounts.length > 0){
            connect(accounts[0].name, am.getPassword(accounts[0]));
        }
        */
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mThread.interrupt();
        mSubscriptions.clear();
        Log.d("XmppService", "onDestroy!");
    }

    public void connect(String userName, String password) {
        if (!mAuthenticated) {
            if (mThread != null) {
                mClient.disconnect();
                mThread.interrupt();
            }
            mThread = new Thread(() -> {
                try {
                    Looper.prepare();
                    mClient.connect(userName, password, XmppService.this, this);
                    Looper.loop();
                } catch (Exception e) {
                    e.printStackTrace();
                    mAuthenticated = false;
                    mClient.disconnect();
                    mThread.interrupt();
                    mHandler.post(() -> mConnectionSubject.onNext(e.getMessage()));
                }
            });
            mThread.start();
        } else {
            mHandler.post(() -> mConnectionSubject.onNext(""));
        }
    }

    public void disconnect() {
        mAuthenticated = false;
        mClient.disconnect();
        mThread.interrupt();
    }

    /*
    public Observable<Collection<RosterEntry>> getRosters() {
        return mClient.getRosters();
    }
    */

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.d("XmppService", "onStartCommand!");
        return super.onStartCommand(intent, flags, startId);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d("XmppService", "onBind!");
        mBinded = true;
        return new XmppBinder();
    }

    @Override
    public void onRebind(Intent intent) {
        Log.d("XmppService", "onRebind!");
        mBinded = true;
        super.onRebind(intent);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d("XmppService", "onUnbind!");
        mBinded = false;
        return true;//super.onUnbind(intent);
    }

    @Override
    public void connected(final XMPPConnection connection) {
        Log.d("xmpp", "Connected!");
    }

    @Override
    public void connectionClosed() {
        Log.d("xmpp", "Closed!");
        mAuthenticated = false;
        mThread.interrupt();
        mHandler.post(() -> mConnectionSubject.onNext("Connection Closed"));
    }

    @Override
    public void connectionClosedOnError(Exception arg0) {
        Log.d("xmpp", "ConnectionClosedOn Error!");
        mAuthenticated = false;
        mThread.interrupt();
        mHandler.post(() -> mConnectionSubject.onNext(arg0.getMessage()));
    }

    @Override
    public void reconnectingIn(int arg0) {
        Log.d("xmpp", "Reconnectingin " + arg0);
    }

    @Override
    public void reconnectionFailed(final Exception arg0) {
        Log.d("xmpp", "ReconnectionFailed!");
        mAuthenticated = false;
        mHandler.post(() -> mConnectionSubject.onNext(arg0.getMessage()));
    }

    @Override
    public void reconnectionSuccessful() {
        Log.d("xmpp", "ReconnectionSuccessful");
    }

    @Override
    public void authenticated(XMPPConnection arg0, boolean arg1) {
        Log.d("xmpp", "Authenticated!");
        mAuthenticated = true;
        mHandler.post(() -> mConnectionSubject.onNext(""));
    }

    private void subsrcubeToMessages() {
        mSubscriptions.clear();
        Subscription subscription = XmppChatManager.getInstance().subscribe2message(
                message -> {
                    if (!mBinded) {
                        sendNotification(message);
                    }
                },
                throwable -> throwable.printStackTrace(),
                () -> {
                }
        );
        mSubscriptions.add(subscription);
    }

    private void sendNotification(MessageItem messageItem) {
        Subscription subscription = mDatabase.getOrmUser(messageItem.getUserName())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        messageItems -> {
                            Bitmap bitmap = mImageUtils.getBitmap(messageItems.getAvatar(), 100, 100);
                            Bitmap roundedBitmap = mImageUtils.getRoundedBitmap(bitmap, 100, 100);
                            Intent intent = new Intent(this, ChatActivity.class);
                            intent.putExtra(ChatActivity.CHAT_NAME, messageItem.getUserName());
                            PendingIntent pIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
                            notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                            notificationManager.notify(1, buildNotification(roundedBitmap, messageItem.getUserName(),
                                    messageItem.getMessage(), pIntent));
                        },
                        throwable -> throwable.printStackTrace()
                );
        mSubscriptions.add(subscription);

    }

    public Notification buildNotification(Bitmap avatar, String title, String content, PendingIntent intent) {
        return new NotificationCompat.Builder(this)
                .setContentTitle(title)
                .setContentText(content)
                .setSmallIcon(R.drawable.ic_mail_outline_black_24dp)
                .setLargeIcon(avatar)
                .setContentIntent(intent)
                .setAutoCancel(true)
                .setDefaults(Notification.DEFAULT_SOUND | Notification.DEFAULT_VIBRATE)
                .build();

    }
}
