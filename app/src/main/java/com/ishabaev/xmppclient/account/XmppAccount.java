package com.ishabaev.xmppclient.account;

import android.accounts.Account;
import android.os.Parcel;

/**
 * Created by ishabaev on 12.07.16.
 */
public class XmppAccount extends Account {

    public static final String TYPE = "com.ishabaev.xmppclient";

    public static final String TOKEN_FULL_ACCESS = "com.ishabaev.xmppclient.TOKEN_FULL_ACCESS";

    public static final String KEY_PASSWORD = "com.ishabaev.xmppclient.KEY_PASSWORD";

    public XmppAccount(Parcel in) {
        super(in);
    }

    public XmppAccount(String name) {
        super(name, TYPE);
    }

    public static final Creator<XmppAccount> CREATOR = new Creator<XmppAccount>() {
        public XmppAccount createFromParcel(Parcel source) {
            return new XmppAccount(source);
        }

        public XmppAccount[] newArray(int size) {
            return new XmppAccount[size];
        }
    };
}