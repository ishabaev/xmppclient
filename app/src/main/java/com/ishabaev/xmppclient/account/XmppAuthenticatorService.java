package com.ishabaev.xmppclient.account;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

/**
 * Created by ishabaev on 12.07.16.
 */
public class XmppAuthenticatorService extends Service{

    private XmppAuthenticator mAuthenticator;

    @Override
    public void onCreate() {
        super.onCreate();
        mAuthenticator = new XmppAuthenticator(getApplicationContext());
    }

    @Override
    public IBinder onBind(Intent intent) {
        return mAuthenticator.getIBinder();
    }
}
