package com.ishabaev.xmppclient.chat;

import android.accounts.AccountManager;

import com.ishabaev.xmppclient.XmppService;
import com.ishabaev.xmppclient.data.MessageItem;

import org.jivesoftware.smackx.chatstates.ChatState;

import java.util.List;

import rx.Observable;

/**
 * Created by ishabaev on 10.07.16.
 */
public interface ChatContract {

    interface View{

        void addMessage(MessageItem message);

        void setMessages(List<MessageItem> messages);

        void setTypingText(String text);
    }

    interface Presenter{

        void sendMessage(String chatName, String message);

        void getMessages(String chatName);

        void beginChat(String chat);

        void closeChat(String chat);

        void setChatState(ChatState chatState, String chatName);

        void setService(XmppService service);

        void signIn(AccountManager am);
    }
}
