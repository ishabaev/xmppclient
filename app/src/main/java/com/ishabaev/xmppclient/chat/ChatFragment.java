package com.ishabaev.xmppclient.chat;

import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.ishabaev.xmppclient.R;
import com.ishabaev.xmppclient.XmppChatManager;
import com.ishabaev.xmppclient.XmppDatabase;
import com.ishabaev.xmppclient.XmppService;
import com.ishabaev.xmppclient.data.MessageItem;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ishabaev on 14.07.16.
 */
public class ChatFragment extends Fragment implements ChatContract.View {

    private ImageView mSendButton;
    private EditText mMessage;
    private TextView mTyping;
    private ChatPresenter mPresenter;
    private MessagesAdapter mMessagesAdapter;
    private RecyclerView mRecyclerView;
    public final static String CHAT_NAME = "chat_name";
    private String mChatName;

    private ServiceConnection mServiceConnection;
    private XmppService mService;
    private Intent mIntent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.chat_layout, container, false);

        mPresenter = new ChatPresenter(this, //XmppClientv2.getInstance(),
                XmppChatManager.getInstance(), XmppDatabase.getInstance(getContext()),
                Schedulers.io(), AndroidSchedulers.mainThread());
        if (savedInstanceState == null) {
            mChatName = getArguments().getString(CHAT_NAME);
        } else {
            mChatName = savedInstanceState.getString(CHAT_NAME, "");
        }

        mTyping = (TextView) view.findViewById(R.id.typing);

        mMessage = (EditText) view.findViewById(R.id.message);
        mSendButton = (ImageView) view.findViewById(R.id.send);
        mSendButton.setOnClickListener(v -> {
            mPresenter.sendMessage(mChatName, mMessage.getText().toString());
            mMessage.setText("");
        });
        mRecyclerView = (RecyclerView) view.findViewById(R.id.message_list);
        initRecyclerView();



        mMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                //mPresenter.setChatState(ChatState.composing,mChatName);
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                //mPresenter.setChatState(ChatState.inactive,mChatName);
            }
        });

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIntent = new Intent(getContext(), XmppService.class);
        mServiceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                mService = ((XmppService.XmppBinder) binder).getService();
                mPresenter.setService(mService);
                //mPresenter.signIn(AccountManager.get(getContext()));
            }

            public void onServiceDisconnected(ComponentName name) {

            }
        };
        getActivity().startService(mIntent);
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.beginChat(mChatName);
        mPresenter.getMessages(mChatName);

    }

    @Override
    public void onStart() {
        super.onStart();
        if (mIntent != null) {
            getActivity().bindService(mIntent, mServiceConnection, 0);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unbindService(mServiceConnection);
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(CHAT_NAME, mChatName);
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        layoutManager.setReverseLayout(true);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        mRecyclerView.setLayoutManager(layoutManager);
        mRecyclerView.setItemAnimator(itemAnimator);
        mMessagesAdapter = new MessagesAdapter(new ArrayList<MessageItem>());
        mRecyclerView.setAdapter(mMessagesAdapter);
    }

    @Override
    public void addMessage(MessageItem message) {
        mMessagesAdapter.addMessage(message);
        mRecyclerView.scrollToPosition(0);
    }

    @Override
    public void setMessages(List<MessageItem> messages) {
        mMessagesAdapter.setmMessages(messages);
        mRecyclerView.scrollToPosition(0);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mPresenter.closeChat(mChatName);
    }

    @Override
    public void setTypingText(String text) {
        mTyping.setText(text);
    }
}
