package com.ishabaev.xmppclient.chat;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.os.Handler;

import com.ishabaev.xmppclient.XmppChatManager;
import com.ishabaev.xmppclient.XmppDatabase;
import com.ishabaev.xmppclient.XmppService;
import com.ishabaev.xmppclient.account.XmppAccount;
import com.ishabaev.xmppclient.dao.OrmMessageItem;
import com.ishabaev.xmppclient.data.MessageItem;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smackx.chatstates.ChatState;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import rx.Scheduler;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by ishabaev on 10.07.16.
 */
public class ChatPresenter implements ChatContract.Presenter {

    //private XmppClientv2 mClient;
    private XmppChatManager mChatManager;
    private ChatContract.View mView;
    private Handler myHandler;
    private CompositeSubscription mSubscriptions;
    private Scheduler mBackgroundScheduler;
    private Scheduler mMainScheduler;
    private XmppDatabase mDatabase;
    private XmppService mService;

    public ChatPresenter(ChatContract.View view, //XmppClientv2 client,
                         XmppChatManager chatManager, XmppDatabase database,
                         Scheduler backgroundScheduler, Scheduler mainScheduler) {
        mView = view;
        //mClient = client;
        myHandler = new Handler();
        mSubscriptions = new CompositeSubscription();
        mBackgroundScheduler = backgroundScheduler;
        mMainScheduler = mainScheduler;
        mChatManager = chatManager;
        mDatabase = database;
    }

    @Override
    public void setService(XmppService service) {
        mService = service;
    }

    @Override
    public void signIn(AccountManager am) {
        try {
            mSubscriptions.clear();
            Subscription subscription = mService.subscribe(
                    string -> {
                        if (string.length() == 0) {

                        } else {
                            //mView.showSnackBar(string);
                            //mView.showProgress(false);
                        }
                    },
                    throwable -> {
                        //mView.showSnackBar(throwable.getMessage());
                        //mView.showProgress(false);
                    },
                    () -> {
                    });//mView.showProgress(false));
            mSubscriptions.add(subscription);
            Account[] accounts = am.getAccountsByType(XmppAccount.TYPE);
            mService.connect(accounts[0].name, am.getPassword(accounts[0]));
            //mSubscriptions.add(subscription);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void beginChat(String chat) {
        mSubscriptions.clear();
        mChatManager.beginChat(chat);
        Subscription subscription = mChatManager.subscribe2message(
                messageItem -> mView.addMessage(messageItem),
                throwable -> throwable.printStackTrace(),
                () -> {
                }
        );
        mSubscriptions.add(subscription);


        Subscription stateSubscription = mChatManager.subscribe2state(
                text -> mView.setTypingText(text),
                throwable -> throwable.printStackTrace(),
                () -> {
                }
        );
        mSubscriptions.add(stateSubscription);
    }

    @Override
    public void closeChat(String chat) {
        XmppChatManager.getInstance().closeChat(chat);
        mSubscriptions.clear();
    }

    @Override
    public void setChatState(ChatState chatState, String chatName) {
        try {
            mChatManager.setTyping(chatState, chatName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendMessage(String chatName, String message) {
        try {
            mChatManager.sendMsg(chatName, message);

            final MessageItem messageItem = new MessageItem();
            messageItem.setMessage(message);
            String[] from = "me".split("/");
            if (from.length > 0) {
                messageItem.setUserName(from[0]);
            } else {
                messageItem.setUserName("none");
            }
            messageItem.setMessageDate(Calendar.getInstance().getTime());
            mView.addMessage(messageItem);


            OrmMessageItem ormMessageItem = new OrmMessageItem();
            ormMessageItem.setChatName(chatName);
            ormMessageItem.setUserName(messageItem.getUserName());
            ormMessageItem.setMessage(messageItem.getMessage());
            ormMessageItem.setMessageTime(messageItem.getMessageDate());
            mDatabase.saveOrmMessageItem(ormMessageItem);


        } catch (SmackException.NotConnectedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void getMessages(String chatName) {
        Subscription subscription = mDatabase.getMessages(chatName)
                .map(ormMessageItems -> transformMessages(ormMessageItems))
                .subscribeOn(mBackgroundScheduler)
                .observeOn(mMainScheduler)
                .subscribe(
                        message -> mView.setMessages(message),
                        throwable -> throwable.printStackTrace(),
                        () -> System.out.println("")
                );

        mSubscriptions.add(subscription);
    }

    private List<MessageItem> transformMessages(List<OrmMessageItem> ormMessageItems) {
        List<MessageItem> messageList = new ArrayList<>();
        for (OrmMessageItem omi : ormMessageItems) {
            MessageItem message = new MessageItem();
            message.setMessage(omi.getMessage());
            message.setUserName(omi.getUserName());
            message.setMessageDate(omi.getMessageTime());
            messageList.add(message);
        }
        return messageList;
    }
}
