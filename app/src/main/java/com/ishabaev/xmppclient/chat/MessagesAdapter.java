package com.ishabaev.xmppclient.chat;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ishabaev.xmppclient.R;
import com.ishabaev.xmppclient.data.MessageItem;

import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by ishabaev on 10.07.16.
 */
public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.ViewHolder> {

    public interface MessageListAdapterItemListener {

        void onItemClick(MessageItem message);
    }

    private List<MessageItem> mMessages;
    private MessageListAdapterItemListener mListener;

    public MessagesAdapter(List<MessageItem> messages) {
        mMessages = messages;
    }

    public void setListener(MessageListAdapterItemListener listener) {
        this.mListener = listener;
    }

    public void addMessage(MessageItem message) {
        mMessages.add(0, message);
        //notifyItemChanged(0);
        notifyDataSetChanged();
    }

    public void setmMessages(List<MessageItem> messages) {
        mMessages.clear();
        mMessages.addAll(messages);
        //notifyItemChanged(0);
        notifyDataSetChanged();
    }

    public void addMessages(List<MessageItem> messages) {
        mMessages.addAll(mMessages.size(), messages);
        notifyDataSetChanged();
    }

    public void clear() {
        mMessages.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.view.setOnClickListener(v -> {
            if (mListener != null) {
                mListener.onItemClick(mMessages.get(holder.getAdapterPosition()));
            }
        });
        holder.userName.setText(mMessages.get(position).getUserName());
        holder.message.setText(mMessages.get(position).getMessage());
        SimpleDateFormat format = new SimpleDateFormat("HH:mm");
        String value = format.format(mMessages.get(position).getMessageDate());
        holder.messageDate.setText(value);
    }

    @Override
    public int getItemCount() {
        return mMessages.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public View view;
        public TextView userName;
        public TextView message;
        public TextView messageDate;
        public ImageView userIcon;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            this.userName = (TextView) view.findViewById(R.id.username);
            this.message = (TextView) view.findViewById(R.id.message);
            this.messageDate = (TextView) view.findViewById(R.id.message_time);
            this.userIcon = (ImageView) view.findViewById(R.id.user_icon);
        }
    }
}
