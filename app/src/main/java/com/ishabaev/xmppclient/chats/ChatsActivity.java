package com.ishabaev.xmppclient.chats;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import com.ishabaev.xmppclient.R;
import com.ishabaev.xmppclient.XmppChatManager;
import com.ishabaev.xmppclient.XmppDatabase;
import com.ishabaev.xmppclient.XmppService;
import com.ishabaev.xmppclient.account.XmppAccount;
import com.ishabaev.xmppclient.chat.ChatActivity;
import com.ishabaev.xmppclient.data.ChatItem;
import com.ishabaev.xmppclient.login.SignInActivity;

import java.util.ArrayList;
import java.util.List;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class ChatsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, ChatsContract.View {

    private ChatsAdapter mChatsAdapter;
    private ChatsContract.Presenter mPresenter;
    public final static String USER_NAME = "user_name";
    private ServiceConnection mServiceConnection;
    private XmppService mService;
    private Intent mIntent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);

        fab.setOnClickListener(v -> System.out.println("Clicked"));


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.chat_list);
        initRecyclerView(recyclerView);

        mPresenter = new ChatsPresenter(this, //XmppClientv2.getInstance(),
                XmppChatManager.getInstance(), XmppDatabase.getInstance(this),
                Schedulers.io(), AndroidSchedulers.mainThread());

        TextView textView = (TextView) navigationView.getHeaderView(0).findViewById(R.id.username);
        textView.setText(getIntent().getStringExtra(USER_NAME));
        AccountManager am = AccountManager.get(this);
        Account[] accounts = am.getAccountsByType(XmppAccount.TYPE);
        if (accounts.length == 0) {
            addNewAccount(am);
        } else {
            mIntent = new Intent(this, XmppService.class);
            mServiceConnection = new ServiceConnection() {
                public void onServiceConnected(ComponentName name, IBinder binder) {
                    mService = ((XmppService.XmppBinder) binder).getService();
                    mPresenter.setService(mService);
                    mPresenter.signIn(am);
                }

                public void onServiceDisconnected(ComponentName name) {

                }
            };
            startService(mIntent);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mIntent != null) {
            bindService(mIntent, mServiceConnection, 0);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mServiceConnection != null) {
            unbindService(mServiceConnection);
        }
    }

    private void addNewAccount(AccountManager am) {
        am.addAccount(XmppAccount.TYPE, XmppAccount.TOKEN_FULL_ACCESS, null, null, this,
                /*
                (future) -> {

                    try {
                        future.getResult();
                    } catch (OperationCanceledException | IOException | AuthenticatorException e) {
                        e.printStackTrace();
                        ChatsActivity.this.finish();
                    }

                }*/null, null
        );
        this.finish();
    }

    private void initRecyclerView(RecyclerView recyclerView) {
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(itemAnimator);
        mChatsAdapter = new ChatsAdapter(new ArrayList<>());
        recyclerView.setAdapter(mChatsAdapter);
        mChatsAdapter.setListener((chat) -> {
            Intent intent = new Intent(ChatsActivity.this, ChatActivity.class);
            intent.putExtra(ChatActivity.CHAT_NAME, chat.getChatName());
            startActivity(intent);
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();


        if (id == R.id.exit) {
            AccountManager am = AccountManager.get(this);
            Account[] accounts = am.getAccountsByType(XmppAccount.TYPE);
            if (accounts.length > 0) {
                am.removeAccount(accounts[0], null, null);
            }
            Intent intent = new Intent(this, SignInActivity.class);
            startActivity(intent);
            mPresenter.disconnect();
            this.finish();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void setChatList(List<ChatItem> items) {
        mChatsAdapter.clear();
        mChatsAdapter.addChats(items);
    }

    @Override
    public void updateChatItem(ChatItem chatItem) {
        mChatsAdapter.updateChatItem(chatItem);
    }
}
