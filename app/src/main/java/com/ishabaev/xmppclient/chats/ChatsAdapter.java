package com.ishabaev.xmppclient.chats;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ishabaev.xmppclient.R;
import com.ishabaev.xmppclient.data.ChatItem;
import com.ishabaev.xmppclient.utils.ImageUtils;

import java.util.List;

/**
 * Created by ishabaev on 10.07.16.
 */
public class ChatsAdapter extends RecyclerView.Adapter<ChatsAdapter.ViewHolder> {

    public interface ChatsAdapterItemListener {

        void onItemClick(ChatItem chatItem);
    }

    private List<ChatItem> mChats;
    private ChatsAdapterItemListener mListener;
    private ImageUtils mImageUtils;

    public ChatsAdapter(List<ChatItem> chatItems) {
        mChats = chatItems;
        mImageUtils = new ImageUtils();
    }

    public void setListener(ChatsAdapterItemListener listener) {
        this.mListener = listener;
    }

    public void addChat(ChatItem chat) {
        mChats.add(mChats.size(), chat);
        notifyItemInserted(mChats.size());
        //notifyDataSetChanged();
    }

    public void addChats(List<ChatItem> chats) {
        mChats.addAll(mChats.size(), chats);
        notifyDataSetChanged();
    }

    public void updateChatItem(ChatItem chatItem) {
        int index = findIndexByName(chatItem.getChatName());
        if (index >= 0) {
            ChatItem chat = mChats.get(index);
            chat.setLastChatMessage(chatItem.getLastChatMessage());
            chat.setLastChatMessageDate(chatItem.getLastChatMessageDate());
            chat.setUnreadMessages(chat.getUnreadMessages() + 1);
            notifyItemChanged(index);
        }
    }

    private int findIndexByName(String chatName) {
        for (int i = 0; i < mChats.size(); i++) {
            if (mChats.get(i).getChatName().equals(chatName)) {
                return i;
            }
        }
        return -1;
    }

    public void clear() {
        mChats.clear();
        notifyDataSetChanged();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.chat_list_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener != null) {
                    mListener.onItemClick(mChats.get(holder.getAdapterPosition()));
                }
            }
        });
        holder.chatName.setText(mChats.get(position).getChatName());
        holder.chatLastMessage.setText(mChats.get(position).getLastChatMessage());
        if (mChats.get(position).getUnreadMessages() > 0) {
            holder.unreadMessages.setText(Integer.toString(mChats.get(position).getUnreadMessages()));
            holder.unreadMessages.setVisibility(View.VISIBLE);
        } else {
            holder.unreadMessages.setVisibility(View.GONE);
        }

        if (mChats.get(position).getIcon() != null) {
            holder.chatIcon.setImageBitmap(mChats.get(position).getIcon());
        }
    }

    @Override
    public int getItemCount() {
        return mChats.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public View view;
        public TextView chatName;
        public TextView chatLastMessage;
        public TextView chatLastMessageDate;
        public ImageView chatIcon;
        public TextView unreadMessages;

        public ViewHolder(View view) {
            super(view);
            this.view = view;
            this.chatName = (TextView) view.findViewById(R.id.chat_name);
            this.chatLastMessage = (TextView) view.findViewById(R.id.chat_last_message);
            this.chatLastMessageDate = (TextView) view.findViewById(R.id.chat_last_message_date);
            this.chatIcon = (ImageView) view.findViewById(R.id.chat_icon);
            unreadMessages = (TextView) view.findViewById(R.id.unread_messages);
        }
    }
}
