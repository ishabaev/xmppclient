package com.ishabaev.xmppclient.chats;

import android.accounts.AccountManager;

import com.ishabaev.xmppclient.XmppService;
import com.ishabaev.xmppclient.data.ChatItem;

import java.util.List;

/**
 * Created by ishabaev on 10.07.16.
 */
public interface ChatsContract {

    interface View {

        void setChatList(List<ChatItem> items);

        void updateChatItem(ChatItem chatItem);
    }

    interface Presenter {

        void signIn(AccountManager am);

        void setService(XmppService service);

        void getChatList();

        void disconnect();
    }
}
