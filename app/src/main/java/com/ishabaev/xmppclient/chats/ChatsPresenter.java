package com.ishabaev.xmppclient.chats;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.graphics.Bitmap;

import com.ishabaev.xmppclient.XmppChatManager;
import com.ishabaev.xmppclient.XmppDatabase;
import com.ishabaev.xmppclient.XmppService;
import com.ishabaev.xmppclient.account.XmppAccount;
import com.ishabaev.xmppclient.dao.OrmUser;
import com.ishabaev.xmppclient.data.ChatItem;
import com.ishabaev.xmppclient.utils.ImageUtils;

import java.util.ArrayList;
import java.util.List;

import rx.Scheduler;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by ishabaev on 10.07.16.
 */
public class ChatsPresenter implements ChatsContract.Presenter {

    //private XmppClientv2 mClient;
    private XmppChatManager mChatManager;
    private ChatsContract.View mView;
    private CompositeSubscription mSubscriptions;
    private Scheduler mBackgroundScheduler;
    private Scheduler mMainScheduler;
    private XmppDatabase mDatabase;
    private XmppService mService;
    private ImageUtils mImageUtils;

    public ChatsPresenter(ChatsContract.View view, //XmppClientv2 client,
                          XmppChatManager chatManager, XmppDatabase database,
                          Scheduler backgroundScheduler, Scheduler mainScheduler) {
        mView = view;
        //mClient = client;
        mSubscriptions = new CompositeSubscription();
        mBackgroundScheduler = backgroundScheduler;
        mMainScheduler = mainScheduler;
        mChatManager = chatManager;
        mDatabase = database;
        mImageUtils = new ImageUtils();
    }

    @Override
    public void getChatList() {
        mSubscriptions.clear();
        Subscription subscription = mDatabase
                .getOrmUsers()
                .flatMap(ormUsers -> {
                    List<ChatItem> chatItems = new ArrayList<>();
                    for (OrmUser ormUser : ormUsers) {
                        ChatItem chatItem = new ChatItem();
                        chatItem.setChatName(ormUser.getUser());
                        byte[] avatar = ormUser.getAvatar();
                        if (avatar != null) {
                            int width = 100;
                            int height = 100;
                            Bitmap bitmap = mImageUtils.getBitmap(avatar, width, width);
                            Bitmap roundedBitmap = mImageUtils.getRoundedBitmap(bitmap, height, height);
                            chatItem.setIcon(roundedBitmap);
                        }
                        chatItems.add(chatItem);
                    }
                    return rx.Observable.just(chatItems);
                })
                .subscribeOn(mBackgroundScheduler)
                .observeOn(mMainScheduler)
                .subscribe(
                        messageItems -> mView.setChatList(messageItems),
                        throwable -> throwable.printStackTrace()
                );
        mSubscriptions.add(subscription);
    }

    public void setService(XmppService service) {
        mService = service;
    }

    @Override
    public void signIn(AccountManager am) {
        try {
            mSubscriptions.clear();
            Subscription subscription = mService.subscribe(
                    string -> {
                        if (string.length() == 0) {
                            getChatList();
                        } else {
                            //mView.showSnackBar(string);
                            //mView.showProgress(false);
                        }
                    },
                    throwable -> {
                        //mView.showSnackBar(throwable.getMessage());
                        //mView.showProgress(false);
                    },
                    () -> {
                    });//mView.showProgress(false));
            mSubscriptions.add(subscription);
            Account[] accounts = am.getAccountsByType(XmppAccount.TYPE);
            mService.connect(accounts[0].name, am.getPassword(accounts[0]));
            //mSubscriptions.add(subscription);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void disconnect() {
        //mClient.disconnect();
    }
}
