package com.ishabaev.xmppclient.dao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table "ORM_MESSAGE".
 */
public class OrmMessage {

    private Long id;
    private String from;
    private String to;
    private String message;
    private java.util.Date messageTime;

    public OrmMessage() {
    }

    public OrmMessage(Long id) {
        this.id = id;
    }

    public OrmMessage(Long id, String from, String to, String message, java.util.Date messageTime) {
        this.id = id;
        this.from = from;
        this.to = to;
        this.message = message;
        this.messageTime = messageTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public java.util.Date getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(java.util.Date messageTime) {
        this.messageTime = messageTime;
    }

}
