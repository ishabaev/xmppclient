package com.ishabaev.xmppclient.dao;

// THIS CODE IS GENERATED BY greenDAO, DO NOT EDIT. Enable "keep" sections if you want to edit. 
/**
 * Entity mapped to table "ORM_MESSAGE_ITEM".
 */
public class OrmMessageItem {

    private Long id;
    private String chatName;
    private String userName;
    private String message;
    private java.util.Date messageTime;

    public OrmMessageItem() {
    }

    public OrmMessageItem(Long id) {
        this.id = id;
    }

    public OrmMessageItem(Long id, String chatName, String userName, String message, java.util.Date messageTime) {
        this.id = id;
        this.chatName = chatName;
        this.userName = userName;
        this.message = message;
        this.messageTime = messageTime;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChatName() {
        return chatName;
    }

    public void setChatName(String chatName) {
        this.chatName = chatName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public java.util.Date getMessageTime() {
        return messageTime;
    }

    public void setMessageTime(java.util.Date messageTime) {
        this.messageTime = messageTime;
    }

}
