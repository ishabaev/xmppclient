package com.ishabaev.xmppclient.data;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ishabaev on 10.07.16.
 */
public class ChatItem {

    @SerializedName("chatName")
    private String mChatName;

    @SerializedName("lastChatMessage")
    private String mLastChatMessage;

    @SerializedName("lastChatMessageDate")
    private Date mLastChatMessageDate;

    @SerializedName("icon")
    private Bitmap mIcon;

    @SerializedName("unreadMessages")
    private int mUnreadMessages;

    public String getChatName() {
        return mChatName;
    }

    public void setChatName(String mChatName) {
        this.mChatName = mChatName;
    }

    public String getLastChatMessage() {
        return mLastChatMessage;
    }

    public void setLastChatMessage(String mLastChatMessage) {
        this.mLastChatMessage = mLastChatMessage;
    }

    public Date getLastChatMessageDate() {
        return mLastChatMessageDate;
    }

    public void setLastChatMessageDate(Date mLastChatMessageDate) {
        this.mLastChatMessageDate = mLastChatMessageDate;
    }

    public Bitmap getIcon() {
        return mIcon;
    }

    public void setIcon(Bitmap mIcon) {
        this.mIcon = mIcon;
    }

    public int getUnreadMessages() {
        return mUnreadMessages;
    }

    public void setUnreadMessages(int mUnreadMessages) {
        this.mUnreadMessages = mUnreadMessages;
    }
}
