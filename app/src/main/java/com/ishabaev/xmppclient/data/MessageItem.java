package com.ishabaev.xmppclient.data;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by ishabaev on 10.07.16.
 */
public class MessageItem {

    @SerializedName("userName")
    private String mUserName;

    @SerializedName("message")
    private String mMessage;

    @SerializedName("messageTime")
    private Date mMessageTime;

    @SerializedName("userIcon")
    private String mIcon;


    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        this.mUserName = userName;
    }

    public String getMessage() {
        return mMessage;
    }

    public void setMessage(String message) {
        this.mMessage = message;
    }

    public Date getMessageDate() {
        return mMessageTime;
    }

    public void setMessageDate(Date messageDate) {
        this.mMessageTime = messageDate;
    }

    public String getIcon() {
        return mIcon;
    }

    public void setIcon(String mIcon) {
        this.mIcon = mIcon;
    }
}
