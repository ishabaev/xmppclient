package com.ishabaev.xmppclient.login;

import com.ishabaev.xmppclient.XmppService;

/**
 * Created by ishabaev on 10.07.16.
 */
public interface SignInContract {

    interface View{

        void showProgress(boolean show);

        void showSnackBar(String text);

        void startMainActivity();

        void showUserNameError(String error);

        void showPasswordError(String error);
    }

    interface Presenter{

        void signIn(String userName, String password, XmppService service);

        void subscribeToService(XmppService service);

        void unsubscribe();
    }
}
