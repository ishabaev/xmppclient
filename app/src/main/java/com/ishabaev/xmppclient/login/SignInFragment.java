package com.ishabaev.xmppclient.login;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.ishabaev.xmppclient.R;
import com.ishabaev.xmppclient.XmppService;
import com.ishabaev.xmppclient.account.XmppAccount;

import java.util.UUID;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by ishabaev on 14.07.16.
 */
public class SignInFragment extends Fragment implements SignInContract.View {

    public final static String EXTRA_TOKEN_TYPE = "com.ishabaev.xmppclient.EXTRA_TOKEN_TYPE";

    private SignInContract.Presenter mPresenter;
    private EditText mUserName;
    private EditText mPassword;
    private RelativeLayout mProgressFrame;
    private ScrollView mScrollView;
    private ServiceConnection mServiceConnection;
    private XmppService mService;
    private Intent mIntent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.sign_in_form, container, false);
        mPresenter = new SignInPresenter(this, //XmppClientv2.getInstance(),
                Schedulers.io(), AndroidSchedulers.mainThread());
        mUserName = (EditText) view.findViewById(R.id.username);
        mPassword = (EditText) view.findViewById(R.id.password);
        Button signIn = (Button) view.findViewById(R.id.signIn);
        if (signIn != null) {
            signIn.setOnClickListener(v -> {
                String username = mUserName.getText().toString();
                String password = mPassword.getText().toString();
                hideKeyboard(null);
                mPresenter.signIn(username, password, mService);
            });
        }
        mProgressFrame = (RelativeLayout) view.findViewById(R.id.progressFrame);
        mScrollView = (ScrollView) view.findViewById(R.id.sign_in_container);
        return view;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mIntent = new Intent(getActivity(), XmppService.class);
        mServiceConnection = new ServiceConnection() {
            public void onServiceConnected(ComponentName name, IBinder binder) {
                mService = ((XmppService.XmppBinder) binder).getService();
                mPresenter.subscribeToService(mService);
            }

            public void onServiceDisconnected(ComponentName name) {

            }
        };
        getActivity().startService(mIntent);
    }


    @Override
    public void onStart() {
        super.onStart();
        getActivity().bindService(mIntent, mServiceConnection, 0);
    }

    @Override
    public void onStop() {
        super.onStop();
        getActivity().unbindService(mServiceConnection);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    private void hideKeyboard(View view) {
        if (view == null) {
            view = getActivity().getCurrentFocus();
        }

        if (view != null) {
            InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void showProgress(boolean show) {
        if (show) {
            hideKeyboard(mProgressFrame);
            mScrollView.setVisibility(View.GONE);
            mProgressFrame.setVisibility(View.VISIBLE);
        } else {
            mProgressFrame.setVisibility(View.GONE);
            mScrollView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void showSnackBar(String text) {
        View view = getActivity().findViewById(R.id.frame);
        if (view != null) {
            Snackbar.make(view, text, Snackbar.LENGTH_LONG).show();
        }
    }

    @Override
    public void showUserNameError(String error) {
        mUserName.setError(error);
    }

    @Override
    public void showPasswordError(String error) {
        mPassword.setError(error);
    }

    @Override
    public void startMainActivity() {
        mPresenter.unsubscribe();
        ((SignInActivity) getActivity()).onTokenReceived(
                new XmppAccount(mUserName.getText().toString()),
                mPassword.getText().toString(), UUID.randomUUID().toString()
        );
    }
}
