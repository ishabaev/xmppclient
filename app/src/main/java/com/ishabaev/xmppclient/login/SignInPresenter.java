package com.ishabaev.xmppclient.login;

import com.ishabaev.xmppclient.XmppService;

import rx.Scheduler;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

/**
 * Created by ishabaev on 10.07.16.
 */
public class SignInPresenter implements SignInContract.Presenter {

    //private XmppClientv2 mClient;
    private CompositeSubscription mSubscriptions;
    private Scheduler mBackgroundScheduler;
    private Scheduler mMainScheduler;
    private SignInContract.View mView;

    public SignInPresenter(SignInContract.View view, //XmppClientv2 client,
                           Scheduler backgroundScheduler, Scheduler mainScheduler) {
        //mClient = client;
        mBackgroundScheduler = backgroundScheduler;
        mMainScheduler = mainScheduler;
        mSubscriptions = new CompositeSubscription();
        mView = view;
    }

    @Override
    public void signIn(String userName, String password, XmppService service) {


        if (userName == null || userName.length() == 0) {
            mView.showUserNameError("User name must not be empty");
            return;
        } else if (userName.split("@").length != 2) {
            mView.showUserNameError("Incorrect username");
            return;
        }

        if (password == null || password.length() == 0) {
            mView.showPasswordError("Password must not be empty");
            return;
        }
        mView.showProgress(true);
        service.connect(userName, password);
        /*
        try {
            Subscription subscription = mClient.subscribe(
                    string -> {
                        if (string.length() == 0) {
                            mView.startMainActivity();
                        } else {
                            mView.showSnackBar(string);
                            mView.showProgress(false);
                        }
                    },
                    throwable -> {
                        mView.showSnackBar(throwable.getMessage());
                        mView.showProgress(false);
                    },
                    () -> mView.showProgress(false)
            );
            mClient.init(userName, password, XmppChatManager.getInstance());
            mSubscriptions.add(subscription);
        } catch (Exception e) {
            e.printStackTrace();
        }
        */
    }

    public void subscribeToService(XmppService service) {
        mSubscriptions.clear();
        Subscription subscription = service.subscribe(
                string -> {
                    if (string.length() == 0) {
                        mView.startMainActivity();
                    } else {
                        mView.showSnackBar(string);
                        mView.showProgress(false);
                    }
                },
                throwable -> {
                    mView.showSnackBar(throwable.getMessage());
                    mView.showProgress(false);
                },
                () -> mView.showProgress(false));
        mSubscriptions.add(subscription);
    }

    @Override
    public void unsubscribe() {
        mSubscriptions.clear();
    }
}
