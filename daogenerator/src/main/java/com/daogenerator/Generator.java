package com.daogenerator;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Schema;

public class Generator {
    public static void main(String[] args) {
        Schema schema = new Schema(16, "com.ishabaev.xmppclient.dao");
        //initChatItem(schema);
        initMessageItem(schema);

        initUser(schema);
        initMessage(schema);

        try {
            new DaoGenerator().generateAll(schema, "app/src/main/java/");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void initChatItem(Schema schema) {
        Entity note = schema.addEntity("OrmChatItem");
        note.addIdProperty();
        note.addStringProperty("chatName");
        note.addStringProperty("lastChatMessage");
        note.addDateProperty("lastChatMessageDate");
        note.addStringProperty("icon");
    }

    private static void initUser(Schema schema) {
        Entity note = schema.addEntity("OrmUser");
        note.addIdProperty();
        note.addStringProperty("user");
        note.addStringProperty("userName");
        note.addStringProperty("resource");
        note.addByteArrayProperty("avatar");
    }

    private static void initMessage(Schema schema) {
        Entity note = schema.addEntity("OrmMessage");
        note.addIdProperty();
        note.addStringProperty("from");
        note.addStringProperty("to");
        note.addStringProperty("message");
        note.addDateProperty("messageTime");
    }

    private static void initMessageItem(Schema schema) {
        Entity note = schema.addEntity("OrmMessageItem");
        note.addIdProperty();
        note.addStringProperty("chatName");
        note.addStringProperty("userName");
        note.addStringProperty("message");
        note.addDateProperty("messageTime");
        //note.addContentProvider();
    }
}
